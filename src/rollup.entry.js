import Vue from 'vue';
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

import VuetifyBtn from './components/VuetifyBtn.vue'

// setup for to loop over and register
const components = {
  VuetifyBtn,
}

// global register components with Vue
function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });
}

const plugin = { install, }

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
  GlobalVue.use(Vuetify)
}


export const strict = false
// export all components by default
export default components
// ecport each component individually
export {
  VuetifyBtn,
}
