# Nuxt-Distill
A Nuxt development environment for Vue + Vuetify components to augment your
Distill publication.


# Nuxt Development
The injection script steps on the toes of the Vue reactivity, especially mount.
Thus Nuxt is used solely for the very comfortable development environment.
Once you are happy with your components, they will be bundled and used for the
static SPA.

## Docker

```bash
# using docker-compose: PRODUCTION
docker-compose -f docker-compose.web.production.yml build
docker-compose -f docker-compose.web.production.yml up
docker-compose -f docker-compose.web.production.yml down

# using docker-compose: DEVELOPMENT
docker-compose -f docker-compose.web.production.yml -f docker-compose.web.development.yml build
docker-compose -f docker-compose.web.production.yml -f docker-compose.web.development.yml up
docker-compose -f docker-compose.web.production.yml -f docker-compose.web.development.yml down

# using the python script: PRODUCTION
python docker.py -w web -p -c build
python docker.py -w web -p -c up
python docker.py -w web -p -c down

# using the python script: DEVELOPMENT
python docker.py -w web -c build
python docker.py -w web -c up
python docker.py -w web -c down
```


## Useful Links

- [distill guide][distill guide]
- [distill post example][distill post example]
- [distill template repo][distill template repo]
- [distill template styles][distill template styles]
- [distill template components][distill template components]




[distill guide]: https://distill.pub/guide/
[distill post example]: https://github.com/distillpub/post--example
[distill template repo]: https://github.com/distillpub/template
[distill template styles]: https://github.com/distillpub/template/tree/master/src/styles
[distill template components]: https://github.com/distillpub/template/tree/master/src/components
